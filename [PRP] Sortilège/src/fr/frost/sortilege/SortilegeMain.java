package fr.frost.sortilege;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.frost.sortilege.listeners.PlayerListener;


public class SortilegeMain extends JavaPlugin {
	
	@Override
	public void onEnable() {

		PluginManager pm = getServer().getPluginManager();
		
		// PLAYER EVENTS
		
		pm.registerEvents(new PlayerListener(), this);
		
	}

}
