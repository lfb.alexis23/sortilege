package fr.frost.sortilege.utils;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.Particle.DustOptions;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class sortilegeManager {
	
    public static Player getTargetPlayer(final Player player) {
        return getTarget(player, player.getWorld().getPlayers());
    }
 
    public static Entity getTargetEntity(final Player player) {
        return getTarget(player, player.getWorld().getEntities());
    }
	
    public static <T extends Entity> T getTarget(final Entity entity,
            final Iterable<T> entities) {
        if (entity == null)
            return null;
        T target = null;
        final double threshold = 1;
        for (final T other : entities) {
            final Vector n = other.getLocation().toVector()
                    .subtract(entity.getLocation().toVector());
            if (entity.getLocation().getDirection().normalize().crossProduct(n)
                    .lengthSquared() < threshold
                    && n.normalize().dot(
                            entity.getLocation().getDirection().normalize()) >= 0) {
                if (target == null
                        || target.getLocation().distanceSquared(
                                entity.getLocation()) > other.getLocation()
                                .distanceSquared(entity.getLocation()))
                    target = other;
            }
        }
        return target;
    }
	
	
	public static void drawLine(Particle particule, Location point1, Location point2, double space) {
	    World world = point1.getWorld();
	    double distance = point1.distance(point2);
	    Vector p1 = point1.toVector();
	    Vector p2 = point2.toVector();
	    Vector vector = p2.clone().subtract(p1).normalize().multiply(space);
	    double length = 0;
	    for (; length < distance; p1.add(vector)) {
	    	if(particule == Particle.REDSTONE) {
		    	DustOptions dustOptions = new DustOptions(Color.fromRGB(0, 127, 255), 1);
		        world.spawnParticle(particule, p1.getX(), p1.getY(), p1.getZ(), 5, dustOptions);
	    	}else {
		        world.spawnParticle(particule, p1.getX(), p1.getY(), p1.getZ(), 1);
	    	}
	        length += space;
	    }
	}

}
