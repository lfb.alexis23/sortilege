package fr.frost.sortilege.listeners;


import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.frost.sortilege.utils.sortilegeManager;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class PlayerListener implements Listener {
	
	HashMap<Player, String> sortilege = new HashMap<>();
	
	@EventHandler
	public void onInteractEvent(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		
		if(event.getItem() != null && event.getItem().getType() == Material.STICK) {
			if(event.getAction().equals(Action.LEFT_CLICK_AIR)) {
				Block block = p.getTargetBlock(null, 100);
				Location bl = block.getLocation();
				Location playerLoc = new Location(p.getWorld(), p.getLocation().getBlockX(), p.getLocation().getBlockY() + 1, p.getLocation().getBlockZ());
				
				if(sortilegeManager.getTargetPlayer(p) != null || sortilegeManager.getTargetEntity(p) != null) {
					Player other = sortilegeManager.getTargetPlayer(p);
					Entity ent = sortilegeManager.getTargetEntity(p);
					
					if(sortilege.get(p) == "amour") {
						other.sendMessage("�dTu es tomb� sur le charme de �7" + p.getName());
						other.getWorld().spawnParticle(Particle.SMOKE_LARGE, other.getLocation().getX(), other.getLocation().getBlockY() + 1, other.getLocation().getZ(), 10);
						other.getWorld().spawnParticle(Particle.HEART, other.getLocation().getX(), other.getLocation().getY() + 3, other.getLocation().getZ(), 10);
					
					}else if(sortilege.get(p) == "levitation") {
						Location point1 = new Location(p.getWorld(), other.getLocation().getX(), other.getLocation().getY() + 0, other.getLocation().getZ());
						Location point2 = new Location(p.getWorld(), other.getLocation().getBlockX(), other.getLocation().getBlockY() + 4, other.getLocation().getBlockZ());
						sortilegeManager.drawLine(Particle.EXPLOSION_HUGE, point1, point2, 1);
						other.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 50, 1));
						
					}else if(sortilege.get(p) == "animal" && ent != null && ent instanceof Animals) {
						Animals animal = (Animals) ent;

						animal.remove();
						ent.getWorld().spawnEntity(ent.getLocation(), EntityType.RABBIT);
					}
				
				}
				
				Location playerLoc2 = bl;
				sortilegeManager.drawLine(Particle.REDSTONE, playerLoc, playerLoc2, 1);
				
			}else if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				if(!sortilege.containsKey(p)) {
					sortilege.put(p, "amour");
					p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("�6Sortil�ge: �dAmour"));
				}else if(sortilege.get(p) == "amour") {
					sortilege.put(p, "levitation");
					p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("�6Sortil�ge: �cL�vitation"));
				}else if(sortilege.get(p) == "levitation") {
					sortilege.put(p, "animal");
					p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("�6Sortil�ge: �cAnimal"));
				}else {
					sortilege.put(p, "amour");
					p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("�6Sortil�ge: �dAmour"));
				}
			}
		}
	}
	
}
